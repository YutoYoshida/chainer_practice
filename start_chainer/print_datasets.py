#! /usr/bin/env python
import numpy as np
from chainer.datasets import tuple_dataset
from PIL import Image
import glob
import matplotlib.pyplot as plt
import chainer
from chainer.datasets import mnist
from chainer.datasets import split_dataset_random
import chainer.links as L
import chainer.functions as F

##################################################
train, test = mnist.get_mnist(withlabel=True, ndim=1)
x, t = train[0]
plt.imshow(x.reshape(28, 28), cmap='gray')
plt.axis('off')
plt.show()
print(type(x), len(x))
print(type(x), type(t))
print('label:', t, type(t))
##############################################
train_data = []
train_label = []
data_raw = open("data.txt")
#print(data_raw)
count = 0

for line in data_raw:
	#train = np.array([np.float32(int(x) / 255.0) for x in line.split(",")[0:2]])#ラベルを除くデータセットの配列
	train = np.array([np.float32(int(x)) for x in line.split(",")[0:2]])  # ラベルを除くデータセットの配列
	label = np.int32(line.split(",")[2])#ラベルの配列番号を入力
	#print(train)
	#print(label)
	train_data.append(train)
	train_label.append(label)
	count = count + 1

print(train_data,train_label)
print(count)
threshold = np.int32(count)
print(threshold)
train = tuple_dataset.TupleDataset(train_data[0:threshold], train_label[0:threshold])
test = tuple_dataset.TupleDataset(train_data[0:threshold],  train_label[0:threshold])
#threshold = np.int32(len(imageData)/10*9)
#train = tuple_dataset.TupleDataset(imageData[0:threshold], labelData[0:threshold])
#test  = tuple_dataset.TupleDataset(imageData[threshold:],  labelData[threshold:])
