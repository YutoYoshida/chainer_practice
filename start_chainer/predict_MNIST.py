#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import chainer
from chainer.datasets import mnist
from chainer.datasets import split_dataset_random
import chainer.links as L
import chainer.functions as F
from chainer import serializers
from chainer.cuda import to_cpu

train_val, test = mnist.get_mnist(withlabel=True, ndim=1)


class MLP(chainer.Chain):

	def __init__(self, n_mid_units=100, n_out=10):
		super(MLP, self).__init__()

		# パラメータを持つ層の登録
		with self.init_scope():
			self.l1 = L.Linear(None, n_mid_units)
			self.l2 = L.Linear(n_mid_units, n_mid_units)
			self.l3 = L.Linear(n_mid_units, n_out)

	def __call__(self, x):
		# データを受け取った際のforward計算を書く
		h1 = F.relu(self.l1(x))
		h2 = F.relu(self.l2(h1))
		return self.l3(h2)


gpu_id = -1  # CPUを用いる場合は、この値を-1にしてください

infer_net = MLP()
serializers.load_npz('my_mnist.model', infer_net)

if gpu_id >= 0:
	infer_net.to_gpu(gpu_id)

# 1つ目のテストデータを取り出します
x, t = test[0]  # tは使わない

# どんな画像か表示してみます
plt.imshow(x.reshape(28, 28), cmap='gray')
plt.show()

# ミニバッチの形にする（複数の画像をまとめて推論に使いたい場合は、サイズnのミニバッチにしてまとめればよい）
print('元の形：->'+str(x.shape))

x = x[None, ...]

print('ミニバッチの形にしたあと：->'+str(x.shape))

# ネットワークと同じデバイス上にデータを送る
x = infer_net.xp.asarray(x)

# モデルのforward関数に渡す
with chainer.using_config('train', False), chainer.using_config('enable_backprop', False):
	y = infer_net(x)

# Variable形式で出てくるので中身を取り出す
y = y.array

# 結果をCPUに送る
y = to_cpu(y)

# 予測確率の最大値のインデックスを見る
pred_label = y.argmax(axis=1)

print("ネットワークの予測"+str(pred_label[0]))
