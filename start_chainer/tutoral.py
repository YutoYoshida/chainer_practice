import numpy as np
import chainer
from chainer import Function,gradient_check,Variable,optimizers,serializers,utils
import chainer.function as F
import chainer.links as L

#順伝播・逆伝播計算
x_data = np.array([5], dtype = np.float32)
x = Variable(x_data)
print("x_data ->" + str(x_data))
print("x-> " + str(x))
#微分計算
y = x**2 - 2 * x + 1
print(y.array)
y.backward()
