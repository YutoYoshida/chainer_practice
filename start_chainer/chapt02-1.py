import chainer
import chainer.functions as F
import chainer.links as L
from chainer import training, datasets,iterators,optimizers
from chainer.training import extensions
import numpy as np

batch_size = 10 #バッチサイズ
uses_device = -1 #CPUを使用

class MNIST_Conv_NN(chainer.Chain):

	def __init__(self):
		super(MNIST_Conv_NN, self).__init__()
		with self.init_scope():
			self.conv1 = L.Convolution2D(1,8,ksize=3)
			self.liner1 = L.Linear(1352,10)

	def __call__(self,x,t = None,train = True):
		#CNN
		h1 = self.conv1(x) #畳み込み層
		h2 = F.relu(h1) #活性化関数
		h3 = F.max_pooling_2d(h2,2) #プーリング層
		h4 = self.liner1(h3) #全結合層
		#損失か結果を返す
		return F.softmax_cross_entropy(h4,t) if train else F.softmax(h4)

model = MNIST_Conv_NN()

train, test = chainer.datasets.get_mnist(ndim = 3)

train_iter = iterators.SerialIterator(train,batch_size,shuffle=True)
test_iter = iterators.SerialIterator(test,batch_size,repeat=False,shuffle=True)

#誤差伝搬アルゴリズム
optimizer = optimizers.Adam()
optimizer.setup(model)

#デバイスを選択してtrainerを作成
updater = training.StandardUpdater(train_iter, optimizer,device=uses_device)
trainer = training.Trainer(updater,(5,"epoch"),out="result")
#テストをTrainerに設定する
trainer.extend(extensions.Evaluator(test_iter,model,device=uses_device))
#学習の進展を表示
trainer.extend(extensions.ProgressBar())
#正答率を表示
trainer.extend(extensions.PrintReport(["main/accuracy","validation/main/accuracy"]))
trainer.extend(extensions.LogReport())
#実行
trainer.run()
#学習結果を保存
chainer.serializers.save_npz("chap02.npz",model)




