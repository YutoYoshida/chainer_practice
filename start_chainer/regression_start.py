import math
import random
import numpy as np
import pandas as pd
import pandas as pd
import matplotlib.pyplot as plt

from chainer import Chain, Variable
import chainer.functions as F
import chainer.links as L
from chainer import optimizers
from chainer import serializers

#実験データ用の配列
x1 = []
y1 = []
get_value = 0

for i in range(10):
	get_value = random.random()
	x1.append([i])
	y1.append([get_value])

#データフレーム生成
df = pd.DataFrame({"X": x1, "Y": y1})
print(df)

#グラフ表示
plt.plot(x1,y1)
plt.title("Training Data")
plt.xlabel("input_x")
plt.ylabel("output_y")
plt.grid(True)
plt.savefig('data.png')


x = Variable(np.array(x1, dtype = np.float32))
y = Variable(np.array(y1, dtype = np.float32))

class NN(Chain):
	def __init__(self):
		super(NN, self).__init__(
			l1 = L.Linear(1,100),
			l2 = L.Linear(100,50),
			l3 = L.Linear(50,1)
		)

	def predict(self, x):
		h1 = F.relu(self.l1(x))
		h2 = F.relu(self.l2(h1))
		return self.l3(h2)

#ニューラルネットワークモデルの宣言
model = NN()

def forward(x, y, model):
	t = model.predict(x)
	loss = F.mean_squared_error(t,y)
	return loss

optimizer = optimizers.Adam()
optimizer.setup(model)

#学習
loss_list = []
step = []
for i in range(0, 80000):
	print("-------------------------")
	print("現在の学習回数->",i+1)
	loss = forward(x, y, model)
	step.append(i)
	loss_list.append(loss.data)
	print("loss:{}".format(loss.data))
	optimizer.update(forward,x,y,model)
serializers.save_npz('kaiki.model', model)

#学習過程のグラフ
print("学習過程")
plt.plot(step,loss_list)
plt.title("Training Data")
plt.xlabel("EPOCH")
plt.ylabel("loss")
plt.grid(True)
plt.show()

print("推論結果")
# 推論結果
ym = model.predict(x)
plt.plot(x.data, ym.data)
plt.title("Predict")
plt.xlabel("input x")
plt.ylabel("output ym")
plt.grid(True)
plt.savefig('data_result.png')
plt.show()

print("推論結果２")
# 推論結果2
x2 = []
y2 = []
for i in range(10):
	get_value = random.random()
	x2.append([i])
	y2.append([get_value])

xt = Variable(np.array(x2, dtype=np.float32))
yt = model.predict(xt)
plt.plot(xt.data, yt.data, "ro")
plt.title("Predict2")
plt.xlabel("input xt")
plt.ylabel("output yt")
plt.grid(True)
plt.savefig('unknown.png')
plt.show()

print("合体")
plt.plot(x.data, y.data)
plt.plot(x.data, ym.data)
plt.plot(xt.data, yt.data, "ro")
plt.title("comparison")
plt.xlabel("input")
plt.ylabel("output")
plt.grid(True)
plt.savefig('unknown2.png')
plt.show()

df1 = pd.DataFrame({"X": x1, "Y": y1})
df2 = pd.DataFrame({"X": x2, "Y": y2})
print("-------------------------")
print(df1)
print("-------------------------")
print(df2)


"""
predictor = NN()
serializers.load_npz('kaiki.model', predictor)

gpu_id = -1
if gpu_id >= 0:
	predictor.to_gpu(gpu_id)
"""

"""
#推論結果
ym = model.predict(x)
plt.plot(x.data, ym.data)
plt.title("Predict")
plt.xlabel("input x")
plt.ylabel("output ym")
plt.grid(True)
plt.savefig('predict.png')
plt.show()
"""
"""
# 学習過程グラフを一部拡大
plt.plot(step, loss_list)
plt.xlim([50000,56000])
plt.ylim([0,0.02])
plt.title("Training Data")
plt.xlabel("step")
plt.ylabel("loss")
plt.grid(True)
plt.savefig('result.png')
plt.show()
"""






























